import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final HomeController c = Get.put(HomeController());

  HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Counter App'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'You have pushed the button this many times:',
              style: TextStyle(fontSize: 20),
            ),
            Obx(
              () => Text(
                '${c.count}',
                style: Theme.of(context).textTheme.displayMedium,
              ),
            ),
            const SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  color: Colors.blue,
                  child: IconButton(
                    onPressed: () => c.increment(),
                    icon: const Icon(Icons.add),
                  ),
                ),
                Container(
                  color: Colors.red,
                  child: IconButton(
                    onPressed: () => c.decrement(),
                    icon: const Icon(Icons.remove),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
